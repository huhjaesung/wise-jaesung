"""Add version column

Revision ID: ee40ec7c1ee6
Revises: 2336e1b7e3b1
Create Date: 2023-03-06 19:02:56.646615

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "ee40ec7c1ee6"
down_revision = "2336e1b7e3b1"
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "projects",
        sa.Column("version", sa.Integer, nullable=False, server_default="0"),
    )

    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("projects", "version")

    # ### end Alembic commands ###
