# from collections.abc import Iterable
# import enum
import logging
from typing import List, cast
import numpy as np
import torch
from msclap import CLAP

logger = logging.getLogger(__name__)
DEVICE = "cuda" if torch.cuda.is_available() else "cpu"

AVAILABLE_VERSIONS= ['2022', '2023', 'clapcap']



def _load_clap(version: str):
    clap_model = CLAP(version = version, use_cuda=(DEVICE == "cuda"))
    return clap_model


def setup_clap(version: str):

    model = _load_clap(version)

    def get_output_dim():
        """
        Warmup the GPU with these models
        and find the output_dim reliably
        There seems to be no other API in open_clip repo to
        get the output_dim, than running the model
        """
        audio_features = extract_audio_features([DUMMY_FILE])  # How should I put dummy file
        text_features = extract_text_features(["dummy"])
        assert audio_features.shape[1] == text_features.shape[1]
        return audio_features.shape[1]

    def extract_audio_features(audio_paths: List[str]) -> np.ndarray:
        if isinstance(audio_paths, list):
            output = model.get_audio_embeddings(audio_paths).float()
            output /= torch.linalg.norm(output, dim=-1, keepdims=True)
        return output.cpu().numpy()
    
    def extract_text_features(
        queries: List[str],
    ) -> np.ndarray:
        with torch.no_grad():
            output = model.get_text_embeddings(queries).float()
            output /= torch.linalg.norm(output, dim=-1, keepdims=True)

            return output.cpu().numpy()

    output_dim = get_output_dim()
    # Is this right?
    return output_dim, extract_audio_features, extract_text_features


class LinearBinaryClassifier(torch.nn.Module):
    def __init__(self, embedding_dim: int):
        super().__init__()
        self.linear = torch.nn.Linear(embedding_dim, 1)
        self.sigmoid = torch.nn.Sigmoid()

    def forward(self, x):
        return self.sigmoid(self.linear(x))
